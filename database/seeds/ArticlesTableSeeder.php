<?php

use Illuminate\Database\Seeder;
use App\Models\Article;
class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::truncate();
        $faker = \Faker\Factory::create();
        for ($i = 0;$i<40;$i++) {
            Article::create([
                'username' => $faker->name . str_random(5),
                'email' => str_random(10) . '@baidu.com',
                'ipAddress' => '127.0.0.1',
            ]);
        }
    }
}
